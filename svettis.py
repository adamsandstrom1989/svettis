import threading
import requests
import json
from datetime import datetime, timedelta, timezone
from time import sleep


class SvettisApi:
    def __init__(self, user_id, user_pw):
        self.user_id = user_id
        self.user_pw = user_pw
        self.login_info = json.dumps({
            "username": self.user_id,
            "password": self.user_pw
        })

        self.locations = {
            "aspholmen": "1178",
            "city": "1",
            "öster": "380",
            "söder": "915",
        }

        self.all_classes = {
            "aspholmen": list(),
            "city": list(),
            "öster": list(),
            "söder": list()
        }

        self._post_login()
        self.login_struct = dict()

        self.bookable_classes = list()
        self.booked_classes = list()
        self.customer_id = str()
        self.bearer_token = str()


    def _post_login(self):
        """Return True om inlogg lyckas eller kontot redan är inloggat annars False
        
        Loggar in med användar-konto resultat sparas i:
            - self.login_struct (dictionary)
            - self.bearer_token (string)
            - self.customer_id (string)
        """
        
        url = "https://fsorebro.brpsystems.com/brponline/api/ver3/auth/login"
        post_headers = {
            "accept": "application/json, text/plain, */*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US",
            "content-length": str(len(self.login_info)),
            "content-type": "application/json",
            "origin": "https://fsorebro.brponline.se",
            "referer": "https://fsorebro.brponline.se",
            "x-request-source": "brpweb",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.87 Safari/537.36"
        }
        login_data = requests.post(url, headers=post_headers, data=self.login_info)
        if login_data.status_code != 200:
            return False

        self.login_struct = json.loads(login_data.text)
        self.bearer_token = self.login_struct["access_token"]
        self.customer_id = self.login_struct["username"]


    def _retrieve_classes(self, location):
        """Return True om allt lyckas annars False
        
        Hämtar alla tillgängliga friskis & svettis pass för en specifik lokation och sparar i self.all_classes.
        """

        if self.bearer_token is None:
            self._post_login()

        if location not in self.locations.keys():
            print(f"[-] Invalid location: {location}")
            print("Valid locations:")
            for loc in self.locations.keys():
                print(f"\t{loc}")
            return False

        classes_headers = {
            "accept": "application/json, text/plain, */*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US",
            "authorization": f"Bearer {self.bearer_token}",
            "content-type": "application/json",
            "origin": "https://fsorebro.brponline.se",
            "referer": "https://fsorebro.brponline.se",
            "x-request-source": "brpweb",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.87 Safari/537.36",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "sec-gpc": "1"
        }
        timestamp_now = datetime.now(timezone(timedelta(hours=0), "UTC")).isoformat(timespec="milliseconds").split("+")[0]
        timestamp_end = datetime.now(timezone(timedelta(hours=0), "UTC")) + timedelta(days=6)
        timestamp_end = timestamp_end.isoformat(timespec="milliseconds").split("+")[0]

        url = f"https://fsorebro.brpsystems.com/brponline/api/ver3/businessunits/{self.locations[location.lower()]}/groupactivities?period.start={timestamp_now}Z&period.stop={timestamp_end}Z&webCategory=14"
        resp = requests.get(url, headers=classes_headers)

        if resp.status_code != 200:
            print(f"[-] error retrieving classes: HTTP ERROR {resp.status_code}")
            return False
        
        self.all_classes[location] = json.loads(resp.text)
        return True


    def get_supported_locations(self):
        """Returnerar lista med supportade lokationer."""
        return [loc for loc in self.locations.keys()]


    def get_login_info(self):
        """Returnerar en dictionary med konto/sessions information."""
        if len(self.login_struct) == 0:
            self._post_login()
        return self.login_struct


    def get_all_classes(self):
        """Returnerar en dictionary med alla pass för alla supportade lokationer."""
        for location in self.all_classes.keys():
            if len(self.all_classes[location]) == 0:
                self._retrieve_classes(location=location)
        return self.all_classes
    

    def get_classes_for_location(self, location):
        """Returnerar en lista med alla pass för en viss lokation."""
        if len(self.all_classes[location]) == 0:
            self._retrieve_classes(location=location)
        return self.all_classes[location]


    def get_upcoming_classes(self, loc):
        """Returnerar en lista med kommande pass för en viss lokation."""
        upcoming_classes = list()

        if len(self.all_classes[loc]) == 0:
            self._retrieve_classes(location=loc)

        for c in self.all_classes[loc]:
            if c['cancelled'] or c['bookableEarliest'] is None or c['slots']['leftToBook'] == 0:
                continue

            time_start = datetime.fromisoformat(c['bookableEarliest'][:-1])
            time_now = datetime.now(timezone(timedelta(hours=0), "UTC"))

            time_to_start = (time_start - time_now.replace(tzinfo=None)).total_seconds()
            if time_to_start < 0:
                continue
            # Pass som inte startat ännu
            upcoming_classes.append(c)

        return upcoming_classes


    def book_class(self, class_struct):
        """Return text-svar från bokningsförsök
        
        Försöker boka ett pass från strukt som beskriver passet"""
        if len(self.login_struct) == 0:
            self._post_login()
        booking_url = f"https://fsorebro.brpsystems.com/brponline/api/ver3/customers/{self.login_struct['username']}/bookings/groupactivities"
        post_data = {
            "groupActivity": class_struct['id'],
            "allowWaitingList":"true"
        }

        if not class_struct['slots']['hasWaitingList']:
            post_data['allowWaitingList'] = "false"
        post_data = json.dumps(post_data)

        post_headers = {
            "host": "fsorebro.brpsystems.com",
            "accept": "application/json, text/plain, */*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US",
            "authorization": f"Bearer {self.bearer_token}",
            "content-length": str(len(post_data)),
            "content-type": "application/json",
            "origin": "https://fsorebro.brponline.se",
            "referer": "https://fsorebro.brponline.se/",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site",
            "sec-gpc": "1",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.87 Safari/537.36",
            "x-request-source": "brpweb"
        }
        time_start = datetime.fromisoformat(class_struct['bookableEarliest'][:-1])
        time_now = datetime.now(timezone(timedelta(hours=0), "UTC"))
        # time_to_start = (time_start - time_now.replace(tzinfo=None)).total_seconds()
        sleep_time = (time_start-time_now.replace(tzinfo=None)).total_seconds()
        if sleep_time > 0:
            print(f"[+] Sleeping for: {sleep_time} seconds, until booking for class {class_struct['name']} ({class_struct['id']}) is available")
            sleep(sleep_time)

        fail_counter = 0
        nr_tries = 10
        sleep_for = 2
        while True:
            resp = requests.post(booking_url, data=post_data, headers=post_headers)
            resp_dict = json.loads(resp.text)
            if "ALREADY_BOOKED" in resp_dict.values():
                print("[+] Class is already booked returning")
                return None
            if resp.status_code == 201 or fail_counter > nr_tries:
                break
            else:
                print(f"[+] Got error: sleeping for {sleep_for} seconds")
                sleep(sleep_for)
            fail_counter += 1
        return resp.text



if __name__ == "__main__":
    """Exempel användning av svettis som lib."""
    # from svettis import SvettisApi
    import threading
    import yaml

    """Skapa en lokal login.txt fil:
        username: 123456
        password: 123456
    """

    with open("login.txt", "r") as yaml_file:
        try:
            user_account = yaml.safe_load(yaml_file)
        except yaml.YAMLError as exc:
            print(exc)
    
    api = SvettisApi(user_account["username"], user_account["password"])
    
    supported_locations = api.get_supported_locations()
    print("|" + "{:^5}".format("id") + "|" + "{:^20}".format("plats") + "|")
    print("-" * 28)
    loc = -1
    while loc not in range(len(supported_locations)):
        for idx in range(len(supported_locations)):
            print("|" + "{:^5}".format(idx) + "|" + "{:^20}".format(supported_locations[idx]) + "|")
        try:
            loc = int(input("Plats ID: "))
        except Exception as e:
            print(e)
        if loc not in range(len(supported_locations)):
            print(f"{str(loc)} är inte ett giltig ID ange ett ID från listan")
    loc = supported_locations[loc]

    tränings_pass = list()
    for p in api.get_upcoming_classes(loc):
        tränings_pass.append(p)

    idx = 0
    classes_to_book = list()
    print("{:147}".format("_"*147))
    print("|" + "{:^5}".format("ID")+ "| {:^58}".format("Pass") + "| {:^38}".format("Anmälan") + "| {:^38}".format("Instruktör") + "|")
    for c in tränings_pass:
        if not c['cancelled']:
            print("{:147}".format("-"*147))
            print("|{:^5}|{:^59}|{:^39}|{:^39}|".format(idx, c['name'], c['bookableEarliest'][:-1], c['instructors'][0]['name']))
        idx += 1
    print("{:147}".format("-"*147))
    
    while True:
        ans = input("Ange komma separerad lista av pass att boka: ")
        ans = ans.split(",")
        for i in range(len(ans)):
            ans[i] = ans[i].strip()
        for a in ans:
            classes_to_book.append(tränings_pass[int(a)])
            print(f"\n[+] Pass: \"{tränings_pass[int(a)]['name']}\" tillagd i kö.\n")
        break

    threads = list()
    for class_to_book in classes_to_book:
        t = threading.Thread(target=api.book_class, args=[class_to_book])
        t.start()
        threads.append(t)
    for thread in threads:
        t.join()

    print("[+] Done. Exiting.")
